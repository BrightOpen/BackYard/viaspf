mod common;

pub use common::*;
use std::{
    net::{IpAddr, Ipv4Addr},
    thread,
    time::Duration,
};
use viaspf::{evaluate_spf, Config, ErrorCause, LookupError, SpfResult, SpfResultCause};

#[async_std::test]
async fn slow_lookup_times_out() {
    let lookup = MockLookup::builder()
        .lookup_txt(|name| {
            thread::sleep(Duration::from_millis(100));
            match name.as_str() {
                "example.com." => Ok(vec!["v=spf1 a -all".into()]),
                _ => Err(LookupError::NoRecords),
            }
        })
        .lookup_a(|name| match name.as_str() {
            "example.com." => Ok(vec![Ipv4Addr::new(12, 34, 56, 78)]),
            _ => Err(LookupError::NoRecords),
        })
        .build();

    let result = evaluate_spf(
        &lookup,
        &Default::default(),
        IpAddr::from([12, 34, 56, 78]),
        "amy@example.com",
        "mail.example.com",
    )
    .await;

    assert_eq!(result.result, SpfResult::Pass);

    let config = Config::builder().timeout(Duration::from_millis(90)).build();
    let result = evaluate_spf(
        &lookup,
        &config,
        IpAddr::from([12, 34, 56, 78]),
        "amy@example.com",
        "mail.example.com",
    )
    .await;

    assert_eq!(result.result, SpfResult::Temperror);
    assert_eq!(
        result.cause,
        Some(SpfResultCause::Error(ErrorCause::Timeout))
    );
}
