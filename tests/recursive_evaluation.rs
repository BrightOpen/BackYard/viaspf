mod common;

pub use common::*;
use std::net::IpAddr;
use viaspf::{evaluate_spf, ErrorCause, ExplanationString, LookupError, SpfResult, SpfResultCause};

#[async_std::test]
async fn included_and_redirected_query() {
    let lookup = MockLookup::builder()
        .lookup_txt(|name| match name.as_str() {
            "example.com." => Ok(vec!["v=spf1 ~include:x1.example.com -all".into()]),
            "x1.example.com." => Ok(vec!["v=spf1 redirect=x2.example.com".into()]),
            "x2.example.com." => Ok(vec!["v=spf1 redirect=x3.example.com".into()]),
            "x3.example.com." => Ok(vec!["v=spf1 +all".into()]),
            _ => Err(LookupError::NoRecords),
        })
        .build();

    let result = evaluate_spf(
        &lookup,
        &Default::default(),
        IpAddr::from([12, 34, 56, 78]),
        "amy@example.com",
        "mail.example.com",
    )
    .await;

    assert_eq!(result.result, SpfResult::Softfail);
}

#[async_std::test]
async fn redirected_included_query_with_explanation() {
    let lookup = MockLookup::builder()
        .lookup_txt(|name| match name.as_str() {
            "example.com." => Ok(vec![
                "v=spf1 redirect=x1.example.com exp=x0._exp.example.com".into(),
            ]),
            "x1.example.com." => Ok(vec![
                "v=spf1 -include:x2.example.com exp=x1._exp.example.com".into(),
            ]),
            "x2.example.com." => Ok(vec!["v=spf1 +all exp=x2._exp.example.com".into()]),
            "x0._exp.example.com." => panic!(),
            "x1._exp.example.com." => Ok(vec!["explanation%_x1".into()]),
            "x2._exp.example.com." => panic!(),
            _ => Err(LookupError::NoRecords),
        })
        .build();

    let result = evaluate_spf(
        &lookup,
        &Default::default(),
        IpAddr::from([12, 34, 56, 78]),
        "amy@example.com",
        "mail.example.com",
    )
    .await;

    assert_eq!(
        result.result,
        SpfResult::Fail(ExplanationString::Dns("explanation x1".into()))
    );
}

#[async_std::test]
async fn included_spf_record_missing() {
    let lookup = MockLookup::builder()
        .lookup_txt(|name| match name.as_str() {
            "example.com." => Ok(vec!["v=spf1 include:none.example.com -all".into()]),
            _ => Err(LookupError::NoRecords),
        })
        .build();

    let result = evaluate_spf(
        &lookup,
        &Default::default(),
        IpAddr::from([12, 34, 56, 78]),
        "amy@example.com",
        "mail.example.com",
    )
    .await;

    assert_eq!(result.result, SpfResult::Permerror);
    assert_eq!(
        result.cause,
        Some(SpfResultCause::Error(ErrorCause::NoSpfRecord))
    );
}
