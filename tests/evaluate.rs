mod common;

pub use common::*;
use std::net::{IpAddr, Ipv4Addr};
use viaspf::{evaluate_spf, ErrorCause, LookupError, SpfResult, SpfResultCause};

#[async_std::test]
async fn evaluate_invalid_sender() {
    let lookup = MockLookup::builder().lookup_txt(|_| panic!()).build();

    let result = evaluate_spf(
        &lookup,
        &Default::default(),
        IpAddr::from([12, 34, 56, 78]),
        "amy@[12.34.56.78]",
        "mail.example.com",
    )
    .await;

    assert_eq!(result.result, SpfResult::None);
}

#[async_std::test]
async fn evaluate_null_sender() {
    let lookup = MockLookup::builder().lookup_txt(|_| panic!()).build();

    let result = evaluate_spf(
        &lookup,
        &Default::default(),
        IpAddr::from([12, 34, 56, 78]),
        "",
        "mail.example.com",
    )
    .await;

    // An empty MAIL FROM is invalid. Callers must themselves construct a valid
    // sender address in this case, see §2.4.
    assert_eq!(result.result, SpfResult::None);
}

#[async_std::test]
async fn evaluate_simple_match() {
    let lookup = MockLookup::builder()
        .lookup_txt(|name| match name.as_str() {
            "example.com." => Ok(vec!["v=spf1 ip6:2a00:d80:0:f::/64 -all".into()]),
            _ => Err(LookupError::NoRecords),
        })
        .build();

    let result = evaluate_spf(
        &lookup,
        &Default::default(),
        IpAddr::from([0x2a00, 0xd80, 0, 0xf, 0, 0, 0xe, 0x125]),
        "amy@example.com",
        "mail.example.com",
    )
    .await;

    assert_eq!(result.result, SpfResult::Pass);
}

#[async_std::test]
async fn evaluate_sender_without_local_part() {
    let lookup = MockLookup::builder()
        .lookup_txt(|name| match name.as_str() {
            "mail.example.com." => Ok(vec!["v=spf1 a:%{l}@%{o1r}._id.example.com -all".into()]),
            _ => Err(LookupError::NoRecords),
        })
        .lookup_a(|name| match name.as_str() {
            "postmaster@mail._id.example.com." => Ok(vec![Ipv4Addr::new(12, 34, 56, 78)]),
            _ => Err(LookupError::NoRecords),
        })
        .build();

    let result = evaluate_spf(
        &lookup,
        &Default::default(),
        IpAddr::from([12, 34, 56, 78]),
        "mail.example.com",
        "mail.example.com",
    )
    .await;

    assert_eq!(result.result, SpfResult::Pass);
}

#[async_std::test]
async fn evaluate_invalid_helo_domain() {
    let lookup = MockLookup::builder()
        .lookup_txt(|name| match name.as_str() {
            "example.com." => Ok(vec!["v=spf1 +a:%{h}._helo.example.com -all".into()]),
            _ => Err(LookupError::NoRecords),
        })
        .lookup_a(|name| match name.as_str() {
            "mail.example.com._helo.example.com." => Ok(vec![Ipv4Addr::new(12, 34, 56, 78)]),
            "unknown._helo.example.com." => Ok(vec![Ipv4Addr::new(12, 34, 56, 78)]),
            _ => Err(LookupError::NoRecords),
        })
        .build();

    let config = Default::default();

    let result = evaluate_spf(
        &lookup,
        &config,
        IpAddr::from([12, 34, 56, 78]),
        "amy@example.com",
        "mail.example.com",
    )
    .await;

    assert_eq!(result.result, SpfResult::Pass);

    let result = evaluate_spf(
        &lookup,
        &config,
        IpAddr::from([12, 34, 56, 78]),
        "amy@example.com",
        "[12.34.56.78]",
    )
    .await;

    assert_eq!(result.result, SpfResult::Pass);
}

#[async_std::test]
async fn evaluate_sender_with_unicode_domain() {
    let lookup = MockLookup::builder()
        .lookup_txt(|name| match name.as_str() {
            "xn--bcher-kva.de." => Ok(vec!["v=spf1 a:BUECHER.DE -all".into()]),
            _ => Err(LookupError::NoRecords),
        })
        .lookup_a(|name| match name.as_str() {
            "buecher.de." => Ok(vec![Ipv4Addr::new(12, 34, 56, 78)]),
            _ => Err(LookupError::NoRecords),
        })
        .build();

    let result = evaluate_spf(
        &lookup,
        &Default::default(),
        IpAddr::from([12, 34, 56, 78]),
        "emma@Bücher.de",
        "[12.34.56.78]",
    )
    .await;

    assert_eq!(result.result, SpfResult::Pass);
}

#[async_std::test]
async fn spf_record_with_nonmatching_version() {
    let lookup = MockLookup::builder()
        .lookup_txt(|name| match name.as_str() {
            "example.com." => Ok(vec!["v=spf10 -all".into()]),
            _ => Err(LookupError::NoRecords),
        })
        .build();

    let result = evaluate_spf(
        &lookup,
        &Default::default(),
        IpAddr::from([12, 34, 56, 78]),
        "amy@example.com",
        "mail.example.com",
    )
    .await;

    assert_eq!(result.result, SpfResult::None);
}

#[async_std::test]
async fn spf_record_with_invalid_syntax() {
    let lookup = MockLookup::builder()
        .lookup_txt(|name| match name.as_str() {
            "example.com." => Ok(vec!["v=spf1 mx///2 -all".into()]),
            _ => Err(LookupError::NoRecords),
        })
        .build();

    let result = evaluate_spf(
        &lookup,
        &Default::default(),
        IpAddr::from([12, 34, 56, 78]),
        "amy@example.com",
        "mail.example.com",
    )
    .await;

    assert_eq!(result.result, SpfResult::Permerror);
    assert_eq!(
        result.cause,
        Some(SpfResultCause::Error(ErrorCause::InvalidSpfRecordSyntax))
    );
}
