use std::net::{IpAddr, Ipv4Addr, Ipv6Addr};
use viaspf::{evaluate_spf, Lookup, LookupError, LookupResult, Name, SpfResult};

// The ‘extended examples’ from RFC 7208, appendix A.

struct ExampleLookup {
    spf_record: String,
}

impl ExampleLookup {
    fn new<S: Into<String>>(spf_record: S) -> Self {
        Self {
            spf_record: spf_record.into(),
        }
    }
}

#[async_trait::async_trait()]
impl Lookup for ExampleLookup {
    async fn lookup_a(&self, name: &Name) -> LookupResult<Vec<Ipv4Addr>> {
        match name.as_str() {
            "example.com." => Ok(vec![
                Ipv4Addr::new(192, 0, 2, 10),
                Ipv4Addr::new(192, 0, 2, 11),
            ]),
            "amy.example.com." => Ok(vec![Ipv4Addr::new(192, 0, 2, 65)]),
            "bob.example.com." => Ok(vec![Ipv4Addr::new(192, 0, 2, 66)]),
            "mail-a.example.com." => Ok(vec![Ipv4Addr::new(192, 0, 2, 129)]),
            "mail-b.example.com." => Ok(vec![Ipv4Addr::new(192, 0, 2, 130)]),
            "mail-c.example.org." => Ok(vec![Ipv4Addr::new(192, 0, 2, 140)]),
            // Additional records in A.3:
            "mary.mobile-users._spf.example.com." => Ok(vec![Ipv4Addr::new(127, 0, 0, 2)]),
            "fred.mobile-users._spf.example.com." => Ok(vec![Ipv4Addr::new(127, 0, 0, 2)]),
            "15.15.168.192.joel.remote-users._spf.example.com." => {
                Ok(vec![Ipv4Addr::new(127, 0, 0, 2)])
            }
            "16.15.168.192.joel.remote-users._spf.example.com." => {
                Ok(vec![Ipv4Addr::new(127, 0, 0, 2)])
            }
            _ => Err(LookupError::NoRecords),
        }
    }

    async fn lookup_aaaa(&self, _: &Name) -> LookupResult<Vec<Ipv6Addr>> {
        Err(LookupError::NoRecords)
    }

    async fn lookup_mx(&self, name: &Name) -> LookupResult<Vec<Name>> {
        match name.as_str() {
            "example.com." => Ok(vec![
                Name::new("mail-a.example.com").unwrap(),
                Name::new("mail-b.example.com").unwrap(),
            ]),
            "example.org." => Ok(vec![Name::new("mail-c.example.org").unwrap()]),
            _ => Err(LookupError::NoRecords),
        }
    }

    async fn lookup_txt(&self, name: &Name) -> LookupResult<Vec<String>> {
        match name.as_str() {
            "example.com." => Ok(vec![self.spf_record.clone()]),
            // Additional records in A.2 (partial):
            "example.org." => Ok(vec![
                "v=spf1 include:example.com include:example.net -all".into()
            ]),
            "la.example.org." => Ok(vec!["v=spf1 redirect=example.org".into()]),
            // Additional records in A.3:
            "mobile-users._spf.example.com." => Ok(vec!["v=spf1 exists:%{l1r+}.%{d}".into()]),
            "remote-users._spf.example.com." => Ok(vec!["v=spf1 exists:%{ir}.%{l1r+}.%{d}".into()]),
            // Additional records in A.4:
            "ip4._spf.example.com." => Ok(vec!["v=spf1 -ip4:192.0.2.0/24 +all".into()]),
            "ptr._spf.example.com." => {
                // The following record had to be amended from `-ptr` to
                // `-ptr:example.com`, it is incorrect in the RFC. See erratum:
                // https://www.rfc-editor.org/errata/eid6216
                Ok(vec!["v=spf1 -ptr:example.com +all".into()])
            }
            _ => Err(LookupError::NoRecords),
        }
    }

    async fn lookup_ptr(&self, ip: IpAddr) -> LookupResult<Vec<Name>> {
        match ip {
            IpAddr::V4(addr) => match addr.octets() {
                [192, 0, 2, 10] => Ok(vec![Name::new("example.com").unwrap()]),
                [192, 0, 2, 11] => Ok(vec![Name::new("example.com").unwrap()]),
                [192, 0, 2, 65] => Ok(vec![Name::new("amy.example.com").unwrap()]),
                [192, 0, 2, 66] => Ok(vec![Name::new("bob.example.com").unwrap()]),
                [192, 0, 2, 129] => Ok(vec![Name::new("mail-a.example.com").unwrap()]),
                [192, 0, 2, 130] => Ok(vec![Name::new("mail-b.example.com").unwrap()]),
                [192, 0, 2, 140] => Ok(vec![Name::new("mail-c.example.org").unwrap()]),
                [10, 0, 0, 4] => Ok(vec![Name::new("bob.example.com").unwrap()]),
                _ => Err(LookupError::NoRecords),
            },
            IpAddr::V6(_) => Err(LookupError::NoRecords),
        }
    }
}

const IP_10: IpAddr = IpAddr::V4(Ipv4Addr::new(192, 0, 2, 10));
const IP_11: IpAddr = IpAddr::V4(Ipv4Addr::new(192, 0, 2, 11));
const IP_65: IpAddr = IpAddr::V4(Ipv4Addr::new(192, 0, 2, 65));
const IP_129: IpAddr = IpAddr::V4(Ipv4Addr::new(192, 0, 2, 129));
const IP_130: IpAddr = IpAddr::V4(Ipv4Addr::new(192, 0, 2, 130));
const IP_140: IpAddr = IpAddr::V4(Ipv4Addr::new(192, 0, 2, 140));
const IP_X: IpAddr = IpAddr::V4(Ipv4Addr::new(1, 2, 3, 4));
const EXAMPLE_COM: &str = "example.com";
const AMY_EXAMPLE_COM: &str = "amy@example.com";

#[async_std::test]
async fn simple_examples_1() {
    let lookup = ExampleLookup::new("v=spf1 +all");
    pass(&lookup, IP_10, AMY_EXAMPLE_COM).await;
    pass(&lookup, IP_X, AMY_EXAMPLE_COM).await;
}

#[async_std::test]
async fn simple_examples_2() {
    let lookup = ExampleLookup::new("v=spf1 a -all");
    pass(&lookup, IP_10, AMY_EXAMPLE_COM).await;
    pass(&lookup, IP_11, AMY_EXAMPLE_COM).await;
    fail(&lookup, IP_X, AMY_EXAMPLE_COM).await;
}

#[async_std::test]
async fn simple_examples_3() {
    let lookup = ExampleLookup::new("v=spf1 a:example.org -all");
    fail(&lookup, IP_10, AMY_EXAMPLE_COM).await;
    fail(&lookup, IP_X, AMY_EXAMPLE_COM).await;
}

#[async_std::test]
async fn simple_examples_4() {
    let lookup = ExampleLookup::new("v=spf1 mx -all");
    fail(&lookup, IP_10, AMY_EXAMPLE_COM).await;
    pass(&lookup, IP_129, AMY_EXAMPLE_COM).await;
    pass(&lookup, IP_130, AMY_EXAMPLE_COM).await;
    fail(&lookup, IP_X, AMY_EXAMPLE_COM).await;
}

#[async_std::test]
async fn simple_examples_5() {
    let lookup = ExampleLookup::new("v=spf1 mx:example.org -all");
    fail(&lookup, IP_129, AMY_EXAMPLE_COM).await;
    pass(&lookup, IP_140, AMY_EXAMPLE_COM).await;
    fail(&lookup, IP_X, AMY_EXAMPLE_COM).await;
}

#[async_std::test]
async fn simple_examples_6() {
    let lookup = ExampleLookup::new("v=spf1 mx mx:example.org -all");
    fail(&lookup, IP_10, AMY_EXAMPLE_COM).await;
    pass(&lookup, IP_129, AMY_EXAMPLE_COM).await;
    pass(&lookup, IP_130, AMY_EXAMPLE_COM).await;
    pass(&lookup, IP_140, AMY_EXAMPLE_COM).await;
    fail(&lookup, IP_X, AMY_EXAMPLE_COM).await;
}

#[async_std::test]
async fn simple_examples_7() {
    let lookup = ExampleLookup::new("v=spf1 mx/30 mx:example.org/30 -all");
    pass(&lookup, IP_130, AMY_EXAMPLE_COM).await;
    pass(&lookup, IpAddr::from([192, 0, 2, 131]), AMY_EXAMPLE_COM).await;
    fail(&lookup, IpAddr::from([192, 0, 2, 132]), AMY_EXAMPLE_COM).await;
    pass(&lookup, IP_140, AMY_EXAMPLE_COM).await;
    pass(&lookup, IpAddr::from([192, 0, 2, 142]), AMY_EXAMPLE_COM).await;
    fail(&lookup, IpAddr::from([192, 0, 2, 144]), AMY_EXAMPLE_COM).await;
    fail(&lookup, IP_X, AMY_EXAMPLE_COM).await;
}

#[async_std::test]
async fn simple_examples_8() {
    let lookup = ExampleLookup::new("v=spf1 ptr -all");
    pass(&lookup, IP_65, AMY_EXAMPLE_COM).await;
    fail(&lookup, IP_140, AMY_EXAMPLE_COM).await;
    fail(&lookup, IpAddr::from([10, 0, 0, 4]), AMY_EXAMPLE_COM).await;
}

#[async_std::test]
async fn simple_examples_9() {
    let lookup = ExampleLookup::new("v=spf1 ip4:192.0.2.128/28 -all");
    fail(&lookup, IP_65, AMY_EXAMPLE_COM).await;
    pass(&lookup, IP_129, AMY_EXAMPLE_COM).await;
}

#[async_std::test]
async fn multiple_domain_example() {
    // la.example.org → redirect=example.org → include:example.com → a → match!
    let lookup = ExampleLookup::new("v=spf1 a -all");
    pass(&lookup, IP_10, "me@la.example.org").await;
}

#[async_std::test]
async fn dns_blacklist_style_example() {
    let lookup = ExampleLookup::new(
        "v=spf1 include:mobile-users._spf.%{d} include:remote-users._spf.%{d} -all",
    );

    pass(&lookup, IP_X, "mary+lists@example.com").await;
    fail(&lookup, IP_X, "mallory+lists@example.com").await;

    let ip = IpAddr::from([192, 168, 15, 15]);
    pass(&lookup, ip, "joel+lists@example.com").await;
    fail(&lookup, ip, "mallory+lists@example.com").await;
    fail(&lookup, IP_X, "joel+lists@example.com").await;
}

#[async_std::test]
async fn multiple_requirements_example() {
    let lookup = ExampleLookup::new("v=spf1 -include:ip4._spf.%{d} -include:ptr._spf.%{d} +all");

    pass(&lookup, IP_10, AMY_EXAMPLE_COM).await;
    pass(&lookup, IP_65, AMY_EXAMPLE_COM).await;

    let ip = IpAddr::from([10, 0, 0, 4]);
    fail(&lookup, ip, AMY_EXAMPLE_COM).await;
    fail(&lookup, IP_140, AMY_EXAMPLE_COM).await;
    fail(&lookup, IP_X, AMY_EXAMPLE_COM).await;
}

async fn pass(lookup: &(impl Lookup + Sync + Send), ip: IpAddr, sender: &str) {
    assert_result(SpfResult::Pass, lookup, ip, sender).await
}

async fn fail(lookup: &(impl Lookup + Sync + Send), ip: IpAddr, sender: &str) {
    assert_result(SpfResult::Fail(Default::default()), lookup, ip, sender).await
}

async fn assert_result(
    spf_result: SpfResult,
    lookup: &(impl Lookup + Sync + Send),
    ip: IpAddr,
    sender: &str,
) {
    let result = evaluate_spf(lookup, &Default::default(), ip, sender, EXAMPLE_COM).await;
    assert_eq!(result.result, spf_result);
}
