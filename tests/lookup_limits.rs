mod common;

pub use common::*;
use std::net::{IpAddr, Ipv4Addr};
use viaspf::{evaluate_spf, Config, ErrorCause, LookupError, Name, SpfResult, SpfResultCause};

#[async_std::test]
async fn mx_included_in_global_lookup_count() {
    let lookup = MockLookup::builder()
        .lookup_txt(|name| match name.as_str() {
            "example.com." => Ok(vec!["v=spf1 mx:a.example.com mx:b.example.com -all".into()]),
            _ => Err(LookupError::NoRecords),
        })
        .lookup_mx(|name| match name.as_str() {
            "a.example.com." => Ok(vec![
                Name::new("mxa1.example.com").unwrap(),
                Name::new("mxa2.example.com").unwrap(),
                Name::new("mxa3.example.com").unwrap(),
                Name::new("mxa4.example.com").unwrap(),
            ]),
            "b.example.com." => Ok(vec![
                Name::new("mxb1.example.com").unwrap(),
                Name::new("mxb2.example.com").unwrap(),
                Name::new("mxb3.example.com").unwrap(),
                Name::new("mxb4.example.com").unwrap(),
                Name::new("mxb5.example.com").unwrap(),
            ]),
            _ => Err(LookupError::NoRecords),
        })
        .lookup_a(|name| match name.as_str() {
            "mxa1.example.com." => Ok(vec![Ipv4Addr::new(1, 1, 1, 1)]),
            "mxa2.example.com." => Ok(vec![Ipv4Addr::new(1, 1, 1, 2)]),
            "mxa3.example.com." => Ok(vec![Ipv4Addr::new(1, 1, 1, 3)]),
            "mxa4.example.com." => Ok(vec![Ipv4Addr::new(1, 1, 1, 4)]),
            "mxb1.example.com." => Ok(vec![Ipv4Addr::new(1, 1, 2, 1)]),
            "mxb2.example.com." => Ok(vec![Ipv4Addr::new(1, 1, 2, 2)]),
            "mxb3.example.com." => Ok(vec![Ipv4Addr::new(1, 1, 2, 3)]),
            "mxb4.example.com." => Ok(vec![Ipv4Addr::new(12, 34, 56, 78)]),
            "mxb5.example.com." => Ok(vec![Ipv4Addr::new(1, 1, 2, 5)]),
            _ => Err(LookupError::NoRecords),
        })
        .build();

    let config = Default::default();

    let result = evaluate_spf(
        &lookup,
        &config,
        IpAddr::from([12, 34, 56, 78]),
        "amy@example.com",
        "mail.example.com",
    )
    .await;

    assert_eq!(result.result, SpfResult::Pass);

    let result = evaluate_spf(
        &lookup,
        &config,
        IpAddr::from([1, 1, 2, 5]),
        "amy@example.com",
        "mail.example.com",
    )
    .await;

    assert_eq!(result.result, SpfResult::Permerror);
    assert_eq!(
        result.cause,
        Some(SpfResultCause::Error(ErrorCause::LookupLimitExceeded))
    );
}

#[async_std::test]
async fn mx_per_mech_lookup_limit() {
    let lookup = MockLookup::builder()
        .lookup_txt(|name| match name.as_str() {
            "example.com." => Ok(vec!["v=spf1 mx -all".into()]),
            _ => Err(LookupError::NoRecords),
        })
        .lookup_mx(|name| match name.as_str() {
            "example.com." => Ok(vec![
                Name::new("mx1.example.com").unwrap(),
                Name::new("mx2.example.com").unwrap(),
                Name::new("mx3.example.com").unwrap(),
            ]),
            _ => Err(LookupError::NoRecords),
        })
        .lookup_a(|name| match name.as_str() {
            "mx1.example.com." => Ok(vec![
                Ipv4Addr::new(1, 1, 1, 1),
                Ipv4Addr::new(1, 1, 1, 2),
                Ipv4Addr::new(1, 1, 1, 3),
            ]),
            "mx2.example.com." => Ok(vec![
                Ipv4Addr::new(1, 1, 1, 4),
                Ipv4Addr::new(1, 1, 1, 5),
                Ipv4Addr::new(1, 1, 1, 6),
                Ipv4Addr::new(1, 1, 1, 7),
                Ipv4Addr::new(1, 1, 1, 8),
            ]),
            "mx3.example.com." => Ok(vec![
                Ipv4Addr::new(1, 1, 1, 9),
                Ipv4Addr::new(12, 34, 56, 78),
                Ipv4Addr::new(1, 1, 1, 11),
            ]),
            _ => Err(LookupError::NoRecords),
        })
        .build();

    let config = Default::default();

    let result = evaluate_spf(
        &lookup,
        &config,
        IpAddr::from([12, 34, 56, 78]),
        "amy@example.com",
        "mail.example.com",
    )
    .await;

    assert_eq!(result.result, SpfResult::Pass);

    let result = evaluate_spf(
        &lookup,
        &config,
        IpAddr::from([1, 1, 1, 11]),
        "amy@example.com",
        "mail.example.com",
    )
    .await;

    assert_eq!(result.result, SpfResult::Permerror);
    assert_eq!(
        result.cause,
        Some(SpfResultCause::Error(ErrorCause::LookupLimitExceeded))
    );
}

#[async_std::test]
async fn custom_void_lookup_limit() {
    let lookup = MockLookup::builder()
        .lookup_txt(|name| match name.as_str() {
            "example.com." => Ok(vec![
                "v=spf1 a:x1.com a:x2.com a:x3.com a:x4.com -all".into()
            ]),
            _ => Err(LookupError::NoRecords),
        })
        .build();

    let config = Config::builder().max_void_lookups(3).build();

    let result = evaluate_spf(
        &lookup,
        &config,
        IpAddr::from([12, 34, 56, 78]),
        "amy@example.com",
        "mail.example.com",
    )
    .await;

    assert_eq!(result.result, SpfResult::Permerror);
    assert_eq!(
        result.cause,
        Some(SpfResultCause::Error(ErrorCause::VoidLookupLimitExceeded))
    );

    let config = Config::builder().max_void_lookups(4).build();

    let result = evaluate_spf(
        &lookup,
        &config,
        IpAddr::from([12, 34, 56, 78]),
        "amy@example.com",
        "mail.example.com",
    )
    .await;

    assert_eq!(result.result, SpfResult::Fail(Default::default()));
}
