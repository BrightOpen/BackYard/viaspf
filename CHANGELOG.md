# viaspf changelog

## 0.3.1 (2021-03-10)

*   Fix panic while parsing an invalid SPF record containing a non-ASCII UTF-8
    byte sequence.

## 0.3.0 (2021-01-31)

*   Validate a sender address’s *local-part* more accurately in line with RFC
    5321 (instead of RFC 3696).
*   Rewrite parsing implementation using standard library only, and drop
    dependency on `nom`.
*   Update dev dependency `trust-dns-resolver` to version 0.20.
*   Update dependencies in `Cargo.lock`.
*   Increase minimum supported Rust version to 1.45.0.

## 0.2.1 (2020-12-12)

*   Improve parsing of *domain-spec* tokens. Previously, an SPF record
    containing a mechanism that specified a *domain-spec* ending with a
    *macro-expand* token, followed by a CIDR prefix length (eg,
    `a:mail.%{d}/24`) could not be parsed.

## 0.2.0 (2020-11-03)

*   (breaking change) Require that the closure passed to
    `ConfigBuilder::modify_exp_with` is `Send + Sync`. This allows
    `ConfigBuilder` and `Config` to become `Send` and `Sync`
    ([glts/viaspf#1](https://gitlab.com/glts/viaspf/-/issues/1)).
*   Loosen version requirement for dev dependency `trust-dns-resolver` from an
    exact version (`=`) to the default caret version (`^`). This dependency is
    still in alpha and it makes little sense to pin a moving target.

## 0.1.0 (2020-10-22)

Initial release.
