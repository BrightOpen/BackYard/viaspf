use std::{
    env,
    error::Error,
    net::{IpAddr, Ipv4Addr, Ipv6Addr},
    process,
};
use trust_dns_resolver::{
    error::{ResolveError, ResolveErrorKind},
    Name as TrustDnsName, Resolver,
};
use viaspf::{Config, Lookup, LookupError, LookupResult, Name, SpfResultCause};

struct TrustDnsResolver(Resolver);

#[async_trait::async_trait()]
impl Lookup for TrustDnsResolver {
    async fn lookup_a(&self, name: &Name) -> LookupResult<Vec<Ipv4Addr>> {
        Ok(self
            .0
            .ipv4_lookup(to_trust_dns_name(name)?)
            .map_err(to_lookup_error)?
            .into_iter()
            .collect())
    }
    async fn lookup_aaaa(&self, name: &Name) -> LookupResult<Vec<Ipv6Addr>> {
        Ok(self
            .0
            .ipv6_lookup(to_trust_dns_name(name)?)
            .map_err(to_lookup_error)?
            .into_iter()
            .collect())
    }
    async fn lookup_mx(&self, name: &Name) -> LookupResult<Vec<Name>> {
        let mut mxs = self
            .0
            .mx_lookup(to_trust_dns_name(name)?)
            .map_err(to_lookup_error)?
            .into_iter()
            .collect::<Vec<_>>();
        mxs.sort_by_key(|mx| mx.preference());
        Ok(mxs
            .into_iter()
            .map(|mx| Name::new(&mx.exchange().to_ascii()).map_err(wrap_error))
            .collect::<Result<_, _>>()?)
    }
    async fn lookup_txt(&self, name: &Name) -> LookupResult<Vec<String>> {
        Ok(self
            .0
            .txt_lookup(to_trust_dns_name(name)?)
            .map_err(to_lookup_error)?
            .into_iter()
            .map(|txt| {
                txt.iter()
                    .map(|data| String::from_utf8_lossy(data))
                    .collect()
            })
            .collect())
    }
    async fn lookup_ptr(&self, ip: IpAddr) -> LookupResult<Vec<Name>> {
        Ok(self
            .0
            .reverse_lookup(ip)
            .map_err(to_lookup_error)?
            .into_iter()
            .map(|name| Name::new(&name.to_ascii()).map_err(wrap_error))
            .collect::<Result<_, _>>()?)
    }
}

fn to_trust_dns_name(name: &Name) -> LookupResult<TrustDnsName> {
    TrustDnsName::from_ascii(name).map_err(wrap_error)
}

fn to_lookup_error(error: ResolveError) -> LookupError {
    match error.kind() {
        ResolveErrorKind::NoRecordsFound { .. } => LookupError::NoRecords,
        ResolveErrorKind::Timeout => LookupError::Timeout,
        _ => wrap_error(error),
    }
}

fn wrap_error(error: impl Error + Send + Sync + 'static) -> LookupError {
    LookupError::Dns(Some(error.into()))
}
#[async_std::main]
async fn main() {
    let args = env::args().collect::<Vec<_>>();

    if args.len() != 3 {
        eprintln!("usage: {} <ip> <domain>", args[0]);
        process::exit(1);
    }

    let ip = args[1].parse().unwrap();
    let domain = &args[2];
    let lookup = TrustDnsResolver(Resolver::default().unwrap());
    let config = Config::builder().capture_trace(true).build();

    println!("IP: {}", ip);
    println!("Domain: {}", domain);

    let result = viaspf::evaluate_spf(&lookup, &config, ip, domain, domain).await;

    println!("SPF result: {}", result.result);

    if let Some(cause) = result.cause {
        match cause {
            SpfResultCause::Match(mechanism) => println!("Mechanism: {}", mechanism),
            SpfResultCause::Error(error) => println!("Problem: {}", error),
        }
    }

    if let Some(trace) = result.trace {
        println!("Trace:");
        for event in trace {
            println!("  {}", event.tracepoint);
        }
    }
}
