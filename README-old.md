# viaspf

The **viaspf** library is a complete implementation of the *Sender Policy
Framework* (SPF) protocol, version 1, as specified in [RFC 7208]. It is written
in the Rust programming language.

This library implements the core SPF protocol, but it does not itself do DNS
queries and it does not depend on a DNS library. Users of this library must
provide an implementation of a DNS lookup trait in order to perform SPF queries.
That way, we hope to enable users to choose themselves which DNS resolver they
want to use to implement their SPF verifier applications.

This library was first created in a ‘clean room’ setting. That is, it was
written from scratch, referring only to the RFC, and following it to the letter.
It can therefore be considered an independent alternative to existing SPF
protocol implementations.

The minimum supported Rust version is 1.45.0.

[RFC 7208]: https://www.rfc-editor.org/rfc/rfc7208

## Usage

This is a [Rust] library, for consumption through Cargo as usual.

The main purpose of the viaspf library is SPF verification, that is, checking
whether a sending host is authorised to use some mail domain according to
published SPF policy. For this end, this library provides the function
[`evaluate_spf`] as the main API item. The function signature is as follows:

```rust
fn evaluate_spf(
    lookup: &impl Lookup,
    config: &Config,
    ip: IpAddr,
    sender: &str,
    helo_domain: &str,
) -> QueryResult;
```

Refer to the API documentation for a full description of the inputs and output,
and suggested usage patterns.

The first argument, `lookup`, deserves brief elaboration. While viaspf contains
a complete implementation of the SPF protocol, it does not itself include DNS
resolution capabilities (which are, after all, a core component of SPF).
Instead, DNS resolution is abstracted into the trait [`Lookup`], and is thus
‘pluggable’. `evaluate_spf` requires to be passed an implementation of this
trait, which it will then use as a DNS resolver through which all DNS queries
are conducted. Consult the API documentation for implementation requirements for
the `Lookup` trait, and the included example for a sample implementation.

Note that it is also possible to forgo the SPF evaluation functionality and only
use the library’s data model. The data structures in [`viaspf::record`]
constitute a faithful encoding of the grammar in [section 12] of RFC 7208, and
are mostly decoupled from evaluation.

Finally, we point again to the [API documentation], which describes additional
functionality, such as capturing a trace of the query execution. The trace
provides programmatic access to the protocol processing steps after the fact.

[Rust]: https://www.rust-lang.org
[`evaluate_spf`]: https://docs.rs/viaspf/0.3.1/viaspf/fn.evaluate_spf.html
[`Lookup`]: https://docs.rs/viaspf/0.3.1/viaspf/trait.Lookup.html
[`viaspf::record`]: https://docs.rs/viaspf/0.3.1/viaspf/record/index.html
[section 12]: https://www.rfc-editor.org/rfc/rfc7208#section-12
[API documentation]: https://docs.rs/viaspf

## Examples

A simple SPF verifier is included as an executable example: the command-line
tool `spfquery`. This program uses the [Trust-DNS] resolver to perform DNS
lookups.

Pass an IP address and a domain name as arguments to `spfquery`. The query is
then evaluated and the result and a trace is printed out.

```
cargo run --example spfquery 185.46.57.247 gluet.ch
```

```
IP: 185.46.57.247
Domain: gluet.ch
SPF result: pass
Mechanism: mx
Trace:
  executing SPF query for domain "gluet.ch"
  looking up TXT records for "gluet.ch"
  evaluating SPF record "v=spf1 mx -all"
  evaluating directive "mx"
  evaluating mechanism "mx"
  incrementing global lookup count
  using target name "gluet.ch"
  looking up MX records for "gluet.ch"
  trying MX name "mail.gluet.ch"
  incrementing global lookup count
  looking up A records for "mail.gluet.ch"
  trying IP address 185.46.57.247
  incrementing per-mechanism lookup count
  mechanism matched
  evaluated directive to result "pass"
```

[Trust-DNS]: https://github.com/bluejekyll/trust-dns

## Licence

Copyright © 2020–2021 David Bürgin

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.
