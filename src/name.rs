use std::{
    error::Error,
    fmt::{self, Display, Formatter},
    hash::{Hash, Hasher},
    str::FromStr,
};

/// An error indicating that a domain name could not be parsed.
#[derive(Clone, Copy, Debug, Default, Eq, Hash, PartialEq)]
pub struct ParseNameError;

impl Error for ParseNameError {}

impl Display for ParseNameError {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "could not parse domain name")
    }
}

/// A name that can be used in DNS queries.
///
/// `Name` is a simplistic ad-hoc model for DNS names that should be sufficient
/// for naming purposes as used in the SPF protocol. `Name` plays a part in the
/// DNS name handling of the [`Lookup`] implementation and so the `Name` API is
/// reduced and specific to its use in the evaluation logic.
///
/// The [`new`] and [`domain`] functions create a `Name`. Of these,
/// `Name::domain` is designed for user-supplied inputs, as it will fully encode
/// and validate the input to yield a well-formed ASCII-encoded domain name.
/// `Name::new` is the hands-off variant, it only checks length limits and
/// otherwise accepts all and any string input. This is intended for use with
/// already validated inputs, or inputs that need to be passed along unchanged.
///
/// In the evaluation logic of [`evaluate_spf`], `Name::domain` is used only on
/// the domain name inputs. All further uses of `Name` are through `Name::new`
/// and `Name::as_str`. `Lookup` implementations should treat `Name` as a
/// transparent wrapper of a string.
///
/// A `Name` is always fully-qualified, relative to the root. When borrowing a
/// `Name` with `as_str`, the string has a trailing dot:
///
/// ```
/// # use viaspf::Name;
/// let name = Name::new("my.org")?;
/// assert_eq!(name.as_str(), "my.org.");
/// # Ok::<_, viaspf::ParseNameError>(())
/// ```
///
/// In the `Display` presentation form the trailing dot is omitted:
///
/// ```
/// # use viaspf::Name;
/// let name = Name::new("my.org")?;
/// assert_eq!(name.to_string(), "my.org");
/// # Ok::<_, viaspf::ParseNameError>(())
/// ```
///
/// It needs to be stressed that `Name` is not a general-purpose domain name
/// implementation. For example, escaping of special characters like `.` in
/// labels has not been implemented. The implementation might be improved at a
/// later time if it proves to be insufficient for the purposes of SPF.
///
/// [`Lookup`]: trait.Lookup.html
/// [`new`]: #method.new
/// [`domain`]: #method.domain
/// [`evaluate_spf`]: fn.evaluate_spf.html
#[derive(Clone, Debug)]
pub struct Name(String);

impl Name {
    /// Constructs a syntactically valid DNS name from a string.
    ///
    /// This only checks label length restrictions but does not apply any
    /// transformation to the given string (no case normalisation, Punycode
    /// encoding or similar).
    ///
    /// # Errors
    ///
    /// If the given string is not a valid DNS name, an error is returned.
    ///
    /// # Examples
    ///
    /// ```
    /// use viaspf::Name;
    ///
    /// assert!(Name::new("example.org").is_ok());
    /// assert!(Name::new("_spf.example.org").is_ok());
    /// # Ok::<_, viaspf::ParseNameError>(())
    /// ```
    pub fn new(s: &str) -> Result<Self, ParseNameError> {
        if is_valid_dns_name(&s, false) {
            Ok(Self::root(s.to_owned()))
        } else {
            Err(ParseNameError)
        }
    }

    /// Constructs a syntactically valid, normalised domain name from a string.
    ///
    /// In addition to checking label length restrictions and domain name
    /// syntax, this applies the IDNA algorithm with Punycode encoding to the
    /// given string. Unicode inputs are properly converted to ASCII format.
    ///
    /// # Errors
    ///
    /// If the given string is not a valid domain name, an error is returned.
    ///
    /// # Examples
    ///
    /// ```
    /// use viaspf::Name;
    ///
    /// assert!(Name::domain("example.org").is_ok());
    /// assert!(Name::domain("_spf.example.org").is_err());
    ///
    /// assert_eq!(Name::domain("www.Bücher.de")?.to_string(), "www.xn--bcher-kva.de");
    /// # Ok::<_, viaspf::ParseNameError>(())
    /// ```
    pub fn domain(s: &str) -> Result<Self, ParseNameError> {
        // §4.3: ‘Internationalized domain names MUST be encoded as A-labels’
        let s = idna::domain_to_ascii(s).map_err(|_| ParseNameError)?;
        if is_valid_dns_name(&s, true) {
            Ok(Self::root(s))
        } else {
            Err(ParseNameError)
        }
    }

    // §4.3: ‘Properly formed domains are fully qualified domains […]. That is,
    // in the DNS they are implicitly qualified relative to the root’
    fn root(mut s: String) -> Self {
        if !s.ends_with('.') {
            s.push('.');
        }
        Name(s)
    }

    /// Returns a string slice of the fully-qualified name (including the
    /// trailing dot).
    pub fn as_str(&self) -> &str {
        &self.0
    }

    /// Returns whether a name is a subdomain of another name.
    pub fn is_subdomain_of(&self, other: &Name) -> bool {
        let name = self.as_str();
        let other = other.as_str();
        name.len() > other.len() && {
            let len = name.len() - other.len();
            name[len..].eq_ignore_ascii_case(other) && name[..len].ends_with('.')
        }
    }
}

impl PartialEq for Name {
    fn eq(&self, other: &Self) -> bool {
        self.0.eq_ignore_ascii_case(&other.0)
    }
}

impl Eq for Name {}

impl Hash for Name {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.0.to_ascii_lowercase().hash(state);
    }
}

impl Display for Name {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        self.0[..(self.0.len() - 1)].fmt(f)
    }
}

impl AsRef<str> for Name {
    fn as_ref(&self) -> &str {
        self.as_str()
    }
}

impl FromStr for Name {
    type Err = ParseNameError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Self::new(s)
    }
}

fn is_valid_dns_name(mut s: &str, domain: bool) -> bool {
    if s.ends_with('.') {
        s = &s[..(s.len() - 1)]
    }

    if !has_valid_domain_len(s) {
        return false;
    }

    // As noted in the `Name` documentation, this does not take into account
    // special cases that need escaping, eg `"."`. See RFC 4343.
    let mut labels = s.split('.').rev().peekable();
    if domain {
        if matches!(labels.next(), Some(l) if !is_tld(l)) {
            return false;
        }
        if labels.peek().is_none() {
            return false;
        }
        labels.all(is_label)
    } else {
        labels.all(|l| has_valid_label_len(l) && !l.starts_with('-') && !l.ends_with('-'))
    }
}

// RFC 3696, §2: ‘There is an additional rule that essentially requires that
// top-level domain names not be all-numeric.’
pub fn is_tld(s: &str) -> bool {
    is_label(s) && !s.chars().all(|c: char| c.is_ascii_digit())
}

// ‘The LDH rule, as updated, provides that the labels […] that make up a domain
// name must consist of only the ASCII alphabetic and numeric characters, plus
// the hyphen. […] If the hyphen is used, it is not permitted to appear at
// either the beginning or end of a label.’ And: ‘A DNS label may be no more
// than 63 octets long.’
fn is_label(s: &str) -> bool {
    has_valid_label_len(s)
        && s.starts_with(|c: char| c.is_ascii_alphanumeric())
        && s.ends_with(|c: char| c.is_ascii_alphanumeric())
        && s.chars()
            .all(|c: char| c.is_ascii_alphanumeric() || c == '-')
}

// Maximum domain length without the trailing dot. Implementation note: I have
// also seen the limit placed at 255 [254] (eg RFC 3696), but have not been able
// to find the definitive statement, so going with the more conservative 253 …
pub const MAX_DOMAIN_LENGTH: usize = 253;

fn has_valid_domain_len(s: &str) -> bool {
    matches!(s.len(), 1..=MAX_DOMAIN_LENGTH)
}

fn has_valid_label_len(s: &str) -> bool {
    matches!(s.len(), 1..=63)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn name_new_ok() {
        assert_eq!(Name::new("ch"), Ok(Name("ch.".into())));
        assert_eq!(Name::new(""), Err(ParseNameError));

        assert_eq!(
            Name::new("mail.gluet.ch"),
            Ok(Name("mail.gluet.ch.".into()))
        );
        assert_eq!(
            Name::new("_dmarc.gluet.ch"),
            Ok(Name("_dmarc.gluet.ch.".into()))
        );
        assert_eq!(
            Name::new("185.46.57.247"),
            Ok(Name("185.46.57.247.".into()))
        );
        assert_eq!(Name::new("_--,"), Ok(Name("_--,.".into())));

        assert_eq!(Name::new("ch."), Ok(Name("ch.".into())));
        assert_eq!(
            Name::new("mail.gluet.ch."),
            Ok(Name("mail.gluet.ch.".into()))
        );
        assert_eq!(
            Name::new("_dmarc.gluet.ch."),
            Ok(Name("_dmarc.gluet.ch.".into()))
        );
        assert_eq!(
            Name::new("185.46.57.247."),
            Ok(Name("185.46.57.247.".into()))
        );

        assert_eq!(Name::new(".mail.gluet.ch"), Err(ParseNameError));
        assert_eq!(Name::new("mail.g...t.ch"), Err(ParseNameError));
        assert_eq!(
            Name::new("mail.g   t.ch"),
            Ok(Name("mail.g   t.ch.".into()))
        );

        assert_eq!(
            Name::new("www.Bücher.de"),
            Ok(Name("www.Bücher.de.".into()))
        );
        assert_eq!(
            Name::new("www.xn--bcher-kva.de"),
            Ok(Name("www.xn--bcher-kva.de.".into()))
        );
    }

    #[test]
    fn name_domain_ok() {
        assert_eq!(Name::domain("ch"), Err(ParseNameError));
        assert_eq!(Name::domain("odd-tld-lookalike"), Err(ParseNameError));
        assert_eq!(Name::domain(""), Err(ParseNameError));

        assert_eq!(
            Name::domain("mail.gluet.ch"),
            Ok(Name("mail.gluet.ch.".into()))
        );
        assert_eq!(Name::domain("_dmarc.gluet.ch"), Err(ParseNameError));
        assert_eq!(Name::domain("185.46.57.247"), Err(ParseNameError));

        assert_eq!(
            Name::domain("mail.gluet.ch."),
            Ok(Name("mail.gluet.ch.".into()))
        );
        assert_eq!(Name::domain("_dmarc.gluet.ch."), Err(ParseNameError));
        assert_eq!(Name::domain("185.46.57.247."), Err(ParseNameError));

        assert_eq!(Name::domain(".mail.gluet.ch"), Err(ParseNameError));
        assert_eq!(Name::domain("mail.gluet.ch.."), Err(ParseNameError));

        assert_eq!(
            Name::domain("myhost.local"),
            Ok(Name("myhost.local.".into()))
        );

        assert_eq!(
            Name::domain("www.Bücher.de"),
            Ok(Name("www.xn--bcher-kva.de.".into()))
        );
        assert_eq!(
            Name::domain("www.xn--bcher-kva.de"),
            Ok(Name("www.xn--bcher-kva.de.".into()))
        );
    }

    #[test]
    fn name_normalises_case() {
        assert_eq!(
            Name::domain("www.example.org").unwrap(),
            Name::domain("wWw.ExAmPlE.OrG").unwrap()
        );
    }

    #[test]
    fn is_subdomain_of_ok() {
        let parent = Name::new("example.org").unwrap();
        assert!(Name::new("mail.example.org")
            .unwrap()
            .is_subdomain_of(&parent));
        assert!(!Name::new("example.org").unwrap().is_subdomain_of(&parent));
        assert!(!Name::new("myexample.org").unwrap().is_subdomain_of(&parent));
        assert!(!Name::new("example.com").unwrap().is_subdomain_of(&parent));
        assert!(!Name::new("org").unwrap().is_subdomain_of(&parent));
    }
}
