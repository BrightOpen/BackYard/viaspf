use crate::record::ExplainString;
use std::{
    fmt::{self, Debug, Formatter},
    time::Duration,
};

/// A builder for SPF query configurations.
pub struct ConfigBuilder {
    max_void_lookups: usize,
    timeout: Duration,
    default_explanation: Option<ExplainString>,
    modify_exp_fn: Option<Box<dyn Fn(&mut ExplainString) + Send + Sync>>,
    hostname: Option<String>,
    capture_trace: bool,
}

impl ConfigBuilder {
    /// The maximum number of void lookups allowed. The default is 2.
    pub fn max_void_lookups(mut self, value: usize) -> Self {
        self.max_void_lookups = value;
        self
    }

    /// The duration until a query times out. The default is 20 seconds.
    ///
    /// This is not an accurate, enforced timeout. Rather, the duration is the
    /// minimum amount of time until the library may detect a timeout. If a
    /// `Lookup` implementation itself uses a much longer timeout duration, then
    /// the timeout condition will not be detected until after that timeout has
    /// passed. The time elapsed is checked on every attempt to perform DNS
    /// resolution.
    pub fn timeout(mut self, value: Duration) -> Self {
        self.timeout = value;
        self
    }

    /// The explain string from which to compute the default explanation when a
    /// `Fail` result is returned. The default value is the empty string.
    ///
    /// When the given `ExplainString` is evaluated, it is expanded strictly
    /// locally, without access to the network. Specifically, the `%{p}` macro
    /// therefore always expands to the default value `unknown`.
    pub fn default_explanation<S>(mut self, value: S) -> Self
    where
        S: Into<ExplainString>,
    {
        self.default_explanation = Some(value.into());
        self
    }

    /// The function to apply to explain strings originating from the DNS via an
    /// *exp* modifier.
    ///
    /// This function serves as a callback that allows sanitising or otherwise
    /// altering the third-party-provided explain string before it undergoes
    /// macro expansion. An example use would be prepending a macro string such
    /// as `"%{o} explains: "`.
    pub fn modify_exp_with<F>(mut self, f: F) -> Self
    where
        F: Fn(&mut ExplainString) + Send + Sync + 'static,
    {
        self.modify_exp_fn = Some(Box::new(f));
        self
    }

    /// The hostname of the SPF verifier.
    ///
    /// This is substituted for the `%{r}` macro during macro expansion. The
    /// given string is used as-is, without validation or transformation.
    pub fn hostname<S>(mut self, value: S) -> Self
    where
        S: Into<String>,
    {
        self.hostname = Some(value.into());
        self
    }

    /// Whether to capture a trace during query execution.
    ///
    /// Note: Enabling tracing has a cost, as the data structures encountered
    /// during processing are copied and aggregated in the trace.
    pub fn capture_trace(mut self, value: bool) -> Self {
        self.capture_trace = value;
        self
    }

    /// Constructs a new configuration.
    pub fn build(self) -> Config {
        Config {
            max_void_lookups: self.max_void_lookups,
            timeout: self.timeout,
            default_explanation: self.default_explanation,
            modify_exp_fn: self.modify_exp_fn,
            hostname: self.hostname,
            capture_trace: self.capture_trace,
        }
    }
}

impl Default for ConfigBuilder {
    fn default() -> Self {
        Self {
            // §4.6.4: ‘SPF implementations SHOULD limit "void lookups" to two.
            // An implementation MAY choose to make such a limit configurable.
            // In this case, a default of two is RECOMMENDED.’
            max_void_lookups: 2,
            // ‘MTAs or other processors SHOULD impose a limit on the maximum
            // amount of elapsed time to evaluate check_host(). Such a limit
            // SHOULD allow at least 20 seconds.’
            timeout: Duration::from_secs(20),
            default_explanation: None,
            modify_exp_fn: None,
            hostname: None,
            capture_trace: false,
        }
    }
}

/// An SPF query configuration.
pub struct Config {
    // Note: Keep fields in sync with the `Debug` implementation below.
    max_void_lookups: usize,
    timeout: Duration,
    default_explanation: Option<ExplainString>,
    modify_exp_fn: Option<Box<dyn Fn(&mut ExplainString) + Send + Sync>>,
    hostname: Option<String>,
    capture_trace: bool,
}

impl Config {
    /// Creates a builder for configurations.
    pub fn builder() -> ConfigBuilder {
        Default::default()
    }

    /// The maximum number of void lookups allowed.
    pub fn max_void_lookups(&self) -> usize {
        self.max_void_lookups
    }

    /// The duration until a query times out.
    pub fn timeout(&self) -> Duration {
        self.timeout
    }

    /// The explain string from which to compute the default explanation when a
    /// `Fail` result is returned.
    pub fn default_explanation(&self) -> Option<&ExplainString> {
        self.default_explanation.as_ref()
    }

    /// The function to apply to explain strings originating from the DNS via an
    /// *exp* modifier.
    pub fn modify_exp_fn(&self) -> Option<&(impl Fn(&mut ExplainString) + Send + Sync)> {
        self.modify_exp_fn.as_ref()
    }

    /// The hostname of the SPF verifier.
    pub fn hostname(&self) -> Option<&str> {
        self.hostname.as_deref()
    }

    /// Whether to capture a trace during query execution.
    pub fn capture_trace(&self) -> bool {
        self.capture_trace
    }
}

impl Default for Config {
    fn default() -> Self {
        ConfigBuilder::default().build()
    }
}

// A manual `Debug` implementation is needed to accomodate the `modify_exp_fn`
// closure, but should otherwise be exactly like a derived one.
impl Debug for Config {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        f.debug_struct("Config")
            .field("max_void_lookups", &self.max_void_lookups)
            .field("timeout", &self.timeout)
            .field("default_explanation", &self.default_explanation)
            .field("modify_exp_fn", &"<closure>")
            .field("hostname", &self.hostname)
            .field("capture_trace", &self.capture_trace)
            .finish()
    }
}

impl From<ConfigBuilder> for Config {
    fn from(builder: ConfigBuilder) -> Self {
        builder.build()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn default_void_lookups_ok() {
        assert_eq!(Config::default().max_void_lookups(), 2);
    }

    #[test]
    fn modify_exp_with_ok() {
        let prefix = String::from("%{o} explains: ");
        let config = Config::builder()
            .modify_exp_with(move |explain_string| {
                let prefix = prefix.parse::<ExplainString>().unwrap();
                explain_string.segments.splice(..0, prefix);
            })
            .build();

        let f = config.modify_exp_fn().unwrap();

        let mut explain_string = "not authorized".parse().unwrap();
        f(&mut explain_string);

        assert_eq!(
            explain_string,
            "%{o} explains: not authorized".parse().unwrap()
        );
    }
}
