use crate::name::Name;
use std::{
    error::Error,
    fmt::{self, Display, Formatter},
    net::{IpAddr, Ipv4Addr, Ipv6Addr},
};

/// A result type specialised for lookup errors.
pub type LookupResult<T> = Result<T, LookupError>;

/// Errors that may occur when doing lookups.
#[derive(Debug)]
pub enum LookupError {
    /// The lookup timed out.
    Timeout,
    /// No records were found (NXDOMAIN).
    NoRecords,
    /// Any other error (I/O, encoding, protocol etc.) that causes a DNS lookup
    /// to fail, with source attached. The error cause is made available in the
    /// trace.
    Dns(Option<Box<dyn Error + Send + Sync>>),
}

impl Error for LookupError {
    fn source(&self) -> Option<&(dyn Error + 'static)> {
        match self {
            Self::Timeout | Self::NoRecords => None,
            Self::Dns(e) => e.as_ref().map(|e| e.as_ref() as _),
        }
    }
}

impl Display for LookupError {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        match self {
            Self::Timeout => write!(f, "lookup timed out"),
            Self::NoRecords => write!(f, "no records found"),
            Self::Dns(source) => match source {
                None => write!(f, "lookup returned error"),
                Some(error) => write!(f, "lookup returned error with cause: {}", error),
            },
        }
    }
}

/// A trait for entities that perform DNS resolution.
///
/// This trait uses the [`Name`] type in arguments and return values. See the
/// documentation of this type for a discussion of its design and use.
///
/// [`Name`]: struct.Name.html
#[async_trait::async_trait()]
pub trait Lookup {
    /// Looks up the IPv4 addresses for the given name.
    ///
    /// # Errors
    ///
    /// If the lookup encounters an error, a `LookupError` variant is returned.
    async fn lookup_a(&self, name: &Name) -> LookupResult<Vec<Ipv4Addr>>;

    /// Looks up the IPv6 addresses for the given name.
    ///
    /// # Errors
    ///
    /// If the lookup encounters an error, a `LookupError` variant is returned.
    async fn lookup_aaaa(&self, name: &Name) -> LookupResult<Vec<Ipv6Addr>>;

    /// Looks up the mail exchanger names for the given name.
    ///
    /// Implementers may want to order the returned vector by MX preference.
    ///
    /// # Errors
    ///
    /// If the lookup encounters an error, a `LookupError` variant is returned.
    async fn lookup_mx(&self, name: &Name) -> LookupResult<Vec<Name>>;

    /// Looks up the text records for the given name.
    ///
    /// Implementers should make sure that invalid UTF-8 does not cause the
    /// lookup to fail (eg, using a lossy transformation). A record consisting
    /// of multiple elements should be returned as one string, with elements
    /// joined without a separator.
    ///
    /// # Errors
    ///
    /// If the lookup encounters an error, a `LookupError` variant is returned.
    async fn lookup_txt(&self, name: &Name) -> LookupResult<Vec<String>>;

    /// Looks up the names for the given address (reverse lookup).
    ///
    /// # Errors
    ///
    /// If the lookup encounters an error, a `LookupError` variant is returned.
    async fn lookup_ptr(&self, ip: IpAddr) -> LookupResult<Vec<Name>>;
}

#[cfg(test)]
mod tests {
    use super::*;

    struct MockLookup;

    #[async_trait::async_trait()]
    impl Lookup for MockLookup {
        async fn lookup_a(&self, _name: &Name) -> LookupResult<Vec<Ipv4Addr>> {
            unimplemented!();
        }

        async fn lookup_aaaa(&self, _name: &Name) -> LookupResult<Vec<Ipv6Addr>> {
            unimplemented!();
        }

        async fn lookup_mx(&self, _name: &Name) -> LookupResult<Vec<Name>> {
            unimplemented!();
        }

        async fn lookup_txt(&self, name: &Name) -> LookupResult<Vec<String>> {
            Ok(vec![name.to_string()])
        }

        async fn lookup_ptr(&self, _ip: IpAddr) -> LookupResult<Vec<Name>> {
            unimplemented!();
        }
    }

    #[async_std::test]
    async fn lookup_call_ok() {
        let resolver = MockLookup;

        let v = resolver
            .lookup_txt(&Name::new("gluet.ch").unwrap())
            .await
            .unwrap();
        assert_eq!(v, vec![String::from("gluet.ch")]);
    }
}
