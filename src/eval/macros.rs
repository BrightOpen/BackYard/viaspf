use super::*;
use crate::{
    eval::{
        query::{Query, Resolver},
        terms,
    },
    name::Name,
    record::{
        Delimiters, DomainSpec, Escape, ExplainString, ExplainStringSegment, Macro, MacroExpand,
        MacroKind, MacroString, MacroStringSegment,
    },
    trace::Tracepoint,
};
use std::{fmt::Write, net::IpAddr, num::NonZeroU8, str, time::SystemTime};

#[async_trait::async_trait()]
impl EvaluateToString for DomainSpec {
    async fn evaluate_to_string(
        &self,
        query: &mut Query<'_>,
        resolver: &Resolver<'_>,
    ) -> EvalResult<String> {
        trace!(query, Tracepoint::ExpandDomainSpec(self.clone()));

        self.as_ref().evaluate_to_string(query, resolver).await
    }
}

#[async_trait::async_trait()]
impl EvaluateToString for ExplainString {
    async fn evaluate_to_string(
        &self,
        query: &mut Query<'_>,
        resolver: &Resolver<'_>,
    ) -> EvalResult<String> {
        trace!(query, Tracepoint::ExpandExplainString(self.clone()));

        let mut result = String::new();
        for segment in &self.segments {
            match segment {
                ExplainStringSegment::MacroString(macro_string) => {
                    result += &macro_string.evaluate_to_string(query, resolver).await?;
                }
                space @ ExplainStringSegment::Space => {
                    result += &space.to_string();
                }
            }
        }
        Ok(result)
    }
}

#[async_trait::async_trait()]
impl EvaluateToString for MacroString {
    async fn evaluate_to_string(
        &self,
        query: &mut Query<'_>,
        resolver: &Resolver<'_>,
    ) -> EvalResult<String> {
        let mut result = String::new();
        for segment in &self.segments {
            match segment {
                MacroStringSegment::MacroExpand(macro_expand) => match macro_expand {
                    MacroExpand::Macro(macro_) => {
                        result += &macro_.evaluate_to_string(query, resolver).await?;
                    }
                    MacroExpand::Escape(escape) => {
                        result += match escape {
                            Escape::Percent => "%",
                            Escape::Space => " ",
                            Escape::UrlEncodedSpace => "%20",
                        };
                    }
                },
                MacroStringSegment::MacroLiteral(macro_literal) => {
                    result += macro_literal.as_ref();
                }
            }
        }
        Ok(result)
    }
}
#[async_trait::async_trait()]
impl EvaluateToString for Macro {
    async fn evaluate_to_string(
        &self,
        query: &mut Query<'_>,
        resolver: &Resolver<'_>,
    ) -> EvalResult<String> {
        // §7.2: ‘The following macro letters are allowed only in "exp" text:’,
        // namely ‘c’, ‘r’, and ‘t’.
        use MacroKind::*;
        if !query.state.is_eval_explain_string()
            && matches!(self.kind(), PrettyIp | Receiver | Timestamp)
        {
            return Err(EvalError::InvalidMacroInDomainSpec(self.clone()));
        }
        let mut s = match self.kind() {
            Sender => query.params.sender().to_string(),
            LocalPart => query.params.sender().local_part().to_string(),
            DomainPart => query.params.sender().domain_part().to_string(),
            Domain => query.params.domain().to_string(),
            Ip => to_dot_format_string(query.params.ip()),
            ValidatedDomain => validated_domain_string(query, resolver).await?,
            HeloDomain => to_helo_domain_string(query.params.helo_domain()),
            PrettyIp => query.params.ip().to_string(),
            Receiver => to_hostname_string(query.config.hostname()),
            Timestamp => timestamp_in_secs(),
            VersionLabel => to_version_label_string(query.params.ip()),
        };

        // §7.3: ‘If transformers or delimiters are provided,’ …
        if self.limit().is_some() || self.reverse() || self.delimiters().is_some() {
            // … ‘the replacement value for a macro letter is split into parts
            // separated by one or more of the specified delimiter characters.’
            let mut parts = match self.delimiters() {
                None => split_on_delimiters(&s, &Default::default()),
                Some(delimiters) => split_on_delimiters(&s, delimiters),
            };

            // ‘After performing any reversal operation and/or removal of
            // left-hand parts, the parts are rejoined using "." and not the
            // original splitting characters.’
            if self.reverse() {
                parts.reverse();
            }
            if let Some(limit) = self.limit() {
                truncate_parts(&mut parts, limit);
            }
            s = parts.join(".");
        }

        // ‘Uppercase macros expand exactly as their lowercase equivalents, and
        // are then URL escaped.’
        if self.url_encode() {
            s = url_encode(&s);
        }

        Ok(s)
    }
}

fn to_dot_format_string(addr: IpAddr) -> String {
    match addr {
        IpAddr::V4(addr) => addr.to_string(),
        IpAddr::V6(addr) => addr
            .octets()
            .iter()
            .map(|o| format!("{:x}.{:x}", o >> 4, o & 0xf))
            .collect::<Vec<_>>()
            .join("."),
    }
}

async fn validated_domain_string(
    query: &mut Query<'_>,
    resolver: &Resolver<'_>,
) -> EvalResult<String> {
    let ip = query.params.ip();

    let ptrs = match terms::to_eval_result(resolver.lookup_ptr(query, ip).await) {
        Ok(ptrs) => ptrs,
        // §7.3: ‘if a DNS error occurs, the string "unknown" is used.’
        Err(e) => {
            trace!(query, Tracepoint::ReverseLookupError(e));
            return Ok(default_validated_domain_string());
        }
    };
    terms::increment_void_lookup_count_if_void(query, ptrs.len())?;

    // The only way this function (thus, the `%{p}` macro) may fail is by
    // stepping over a lookup limit, either just above or in the following call:
    let validated_names = terms::get_validated_domain_names(query, resolver, ip, ptrs).await?;

    let domain = query.params.domain();

    // §7.3: ‘If the <domain> is present in the list of validated domains, it
    // SHOULD be used.’
    for name in &validated_names {
        trace!(query, Tracepoint::TryValidatedNameExactMatch(name.clone()));
        if name == domain {
            return Ok(name.to_string());
        }
    }

    // ‘Otherwise, if a subdomain of the <domain> is present, it SHOULD be
    // used.’
    for name in &validated_names {
        trace!(
            query,
            Tracepoint::TryValidatedNameSubdomainMatch(name.clone())
        );
        if name.is_subdomain_of(domain) {
            return Ok(name.to_string());
        }
    }

    // ‘Otherwise, any name from the list can be used. If there are no validated
    // domain names […], the string "unknown" is used.’
    Ok(match validated_names.first() {
        None => {
            trace!(query, Tracepoint::DefaultUnknownValidatedName);
            default_validated_domain_string()
        }
        Some(name) => name.to_string(),
    })
}

pub fn default_validated_domain_string() -> String {
    String::from("unknown")
}

fn to_helo_domain_string(helo_domain: &str) -> String {
    match Name::domain(helo_domain) {
        Ok(name) => name.to_string(),
        Err(_) => {
            // The specification does not elaborate on what should happen when
            // expanding an `%{h}` macro with an invalid HELO domain. This
            // implementation defers evaluation as much as possible, and will
            // here finally fall back to the string ‘unknown’ when no valid
            // domain name can be obtained from the initially supplied input.
            String::from("unknown")
        }
    }
}

// §7.3: ‘This SHOULD be a fully qualified domain name, but if one does not
// exist […] or if policy restrictions dictate otherwise, the word "unknown"
// SHOULD be substituted.’
fn to_hostname_string(hostname: Option<&str>) -> String {
    hostname.map_or_else(|| String::from("unknown"), |s| s.to_owned())
}

fn timestamp_in_secs() -> String {
    SystemTime::now()
        .duration_since(SystemTime::UNIX_EPOCH)
        .expect("system time before UNIX epoch")
        .as_secs()
        .to_string()
}

fn to_version_label_string(addr: IpAddr) -> String {
    match addr {
        IpAddr::V4(_) => "in-addr",
        IpAddr::V6(_) => "ip6",
    }
    .into()
}

fn url_encode(s: &str) -> String {
    let mut result = String::with_capacity(s.len());
    for b in s.bytes() {
        // §7.3: ‘URL escaping MUST be performed for characters not in the
        // "unreserved" set,’ see RFC 3986, §2.3.
        if is_unreserved(b) {
            result.push(b.into());
        } else {
            write!(result, "%{:02X}", b).unwrap();
        }
    }
    result
}

fn is_unreserved(b: u8) -> bool {
    b.is_ascii_alphanumeric() || matches!(b, b'-' | b'.' | b'_' | b'~')
}

fn split_on_delimiters<'a>(s: &'a str, delimiters: &Delimiters) -> Vec<&'a str> {
    // Since delimiters are ASCII-only, working on bytes and turning byte slices
    // back into strings is correct.
    let s = s.as_bytes();
    let delimiters = delimiters.as_ref().as_bytes();

    let mut parts = Vec::new();
    let mut i = 0;
    let mut start = i;

    while i < s.len() {
        if delimiters.contains(&s[i]) {
            parts.push(str::from_utf8(&s[start..i]).unwrap());
            i += 1;
            while i < s.len() && delimiters.contains(&s[i]) {
                i += 1;
            }
            start = i;
        } else {
            i += 1;
        }
    }

    parts.push(str::from_utf8(&s[start..i]).unwrap());

    parts
}

fn truncate_parts<T>(parts: &mut Vec<T>, limit: NonZeroU8) {
    let i = parts.len().saturating_sub(limit.get().into());
    parts.drain(..i);
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::{
        record::{Directive, Mechanism, Record, Term},
        LookupResult,
    };
    use std::net::{IpAddr, Ipv4Addr, Ipv6Addr};

    #[test]
    fn split_on_delimiters_ok() {
        fn split<'a>(s: &'a str, delimiters: &str) -> Vec<&'a str> {
            split_on_delimiters(s, &Delimiters::new(delimiters).unwrap())
        }

        assert_eq!(split("abc", "."), vec!["abc"]);
        assert_eq!(split("", "."), vec![""]);
        assert_eq!(split("abc.", "."), vec!["abc", ""]);
        assert_eq!(split(".abc", "."), vec!["", "abc"]);
        assert_eq!(split(".", "."), vec!["", ""]);

        assert_eq!(split("a,.bc.d", ".,+"), vec!["a", "bc", "d"]);
        assert_eq!(split("+.a,+.", ".,+"), vec!["", "a", ""]);
        assert_eq!(split(",++.,", ".,+"), vec!["", ""]);
        assert_eq!(split("++.,ab", ".,+"), vec!["", "ab"]);
        assert_eq!(split("c,..", ".,+"), vec!["c", ""]);
    }

    #[test]
    fn timestamp_in_secs_ok() {
        let t = timestamp_in_secs();
        assert_eq!(t.len(), 10);
        assert!(t.chars().all(|c| c.is_ascii_digit()));
    }

    #[test]
    fn url_escape_ok() {
        assert_eq!(&url_encode("me@gluet.ch"), "me%40gluet.ch");
        assert_eq!(&url_encode("我@gluet.ch"), "%E6%88%91%40gluet.ch");
    }

    struct MockLookup;

    #[async_trait::async_trait()]
    impl Lookup for MockLookup {
        async fn lookup_a(&self, _name: &Name) -> LookupResult<Vec<Ipv4Addr>> {
            unimplemented!();
        }
        async fn lookup_aaaa(&self, _name: &Name) -> LookupResult<Vec<Ipv6Addr>> {
            unimplemented!();
        }
        async fn lookup_mx(&self, _name: &Name) -> LookupResult<Vec<Name>> {
            unimplemented!();
        }
        async fn lookup_txt(&self, _name: &Name) -> LookupResult<Vec<String>> {
            unimplemented!();
        }
        async fn lookup_ptr(&self, _ip: IpAddr) -> LookupResult<Vec<Name>> {
            unimplemented!();
        }
    }

    // See §7.4.
    #[async_std::test]
    async fn expansion_examples() {
        async fn macro_string(s: &str, query: &mut Query<'_>, resolver: &Resolver<'_>) -> String {
            // Hack: Have to use this roundabout way of creating a macro string,
            // because direct parsing is not part of the public API …
            let mut r = String::from("v=spf1 exists:");
            r += s;
            let macro_string = match r.parse::<Record>().unwrap().terms.get(0) {
                Some(Term::Directive(Directive {
                    mechanism: Mechanism::Exists(exists),
                    ..
                })) => exists.domain_spec.as_ref().to_owned(),
                _ => panic!(),
            };
            macro_string
                .evaluate_to_string(query, resolver)
                .await
                .unwrap()
        }

        let config = Default::default();
        let domain = Name::new("email.example.com").unwrap();
        let sender = Sender::new("strong-bad@email.example.com").unwrap();
        let helo_domain = String::from("mx.example.org");

        let lookup = MockLookup;
        let resolver = Resolver::new(&lookup);

        let ip = Ipv4Addr::new(192, 0, 2, 3);
        let mut query = Query::new(
            &config,
            QueryParams::new(
                ip.into(),
                domain.clone(),
                sender.clone(),
                helo_domain.clone(),
            ),
            Default::default(),
        );

        assert_eq!(
            macro_string("%{s}", &mut query, &resolver).await,
            "strong-bad@email.example.com"
        );
        assert_eq!(
            macro_string("%{o}", &mut query, &resolver).await,
            "email.example.com"
        );
        assert_eq!(
            macro_string("%{d}", &mut query, &resolver).await,
            "email.example.com"
        );
        assert_eq!(
            macro_string("%{d4}", &mut query, &resolver).await,
            "email.example.com"
        );
        assert_eq!(
            macro_string("%{d3}", &mut query, &resolver).await,
            "email.example.com"
        );
        assert_eq!(
            macro_string("%{d2}", &mut query, &resolver).await,
            "example.com"
        );
        assert_eq!(macro_string("%{d1}", &mut query, &resolver).await, "com");
        assert_eq!(
            macro_string("%{dr}", &mut query, &resolver).await,
            "com.example.email"
        );
        assert_eq!(
            macro_string("%{d2r}", &mut query, &resolver).await,
            "example.email"
        );
        assert_eq!(
            macro_string("%{l}", &mut query, &resolver).await,
            "strong-bad"
        );
        assert_eq!(
            macro_string("%{l-}", &mut query, &resolver).await,
            "strong.bad"
        );
        assert_eq!(
            macro_string("%{lr}", &mut query, &resolver).await,
            "strong-bad"
        );
        assert_eq!(
            macro_string("%{lr-}", &mut query, &resolver).await,
            "bad.strong"
        );
        assert_eq!(
            macro_string("%{l1r-}", &mut query, &resolver).await,
            "strong"
        );

        assert_eq!(
            macro_string("%{ir}.%{v}._spf.%{d2}", &mut query, &resolver).await,
            "3.2.0.192.in-addr._spf.example.com"
        );
        assert_eq!(
            macro_string("%{lr-}.lp._spf.%{d2}", &mut query, &resolver).await,
            "bad.strong.lp._spf.example.com"
        );
        assert_eq!(
            macro_string("%{lr-}.lp.%{ir}.%{v}._spf.%{d2}", &mut query, &resolver).await,
            "bad.strong.lp.3.2.0.192.in-addr._spf.example.com"
        );
        assert_eq!(
            macro_string("%{ir}.%{v}.%{l1r-}.lp._spf.%{d2}", &mut query, &resolver).await,
            "3.2.0.192.in-addr.strong.lp._spf.example.com"
        );
        assert_eq!(
            macro_string("%{d2}.trusted-domains.example.net", &mut query, &resolver).await,
            "example.com.trusted-domains.example.net"
        );

        let ip = Ipv6Addr::new(0x2001, 0xdb8, 0, 0, 0, 0, 0, 0xcb01);
        query.params = QueryParams::new(
            ip.into(),
            domain.clone(),
            sender.clone(),
            helo_domain.clone(),
        );
        assert_eq!(
            macro_string("%{ir}.%{v}._spf.%{d2}", &mut query, &resolver).await,
            "1.0.b.c.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.8.b.d.0.1.0.0.2.ip6._spf.example.com"
        );
    }
}
