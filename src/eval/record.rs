use super::*;
use crate::{
    eval::{
        macros,
        query::{Query, Resolver},
    },
    record::{
        ExplainString, ExplainStringSegment, Explanation, MacroExpand, MacroKind, MacroLiteral,
        MacroString, MacroStringSegment, Modifier, Qualifier, Record, Redirect,
    },
    trace::Tracepoint,
};

#[async_trait::async_trait()]
impl Evaluate for Record {
    async fn evaluate(&self, query: &mut Query<'_>, resolver: &Resolver<'_>) -> SpfResult {
        trace!(query, Tracepoint::EvaluateRecord(self.clone()));

        // §6: ‘These two modifiers MUST NOT appear in a record more than once
        // each. If they do, then check_host() exits with a result of
        // "permerror".’
        let redirect = match get_redirect(self) {
            Ok(r) => r,
            Err(redirects) => {
                trace!(query, Tracepoint::MultipleRedirectModifiers(redirects));
                query.result_cause = Some(ErrorCause::InvalidSpfRecordSyntax.into());
                return SpfResult::Permerror;
            }
        };
        let explanation = match get_explanation(self) {
            Ok(e) => e,
            Err(explanations) => {
                trace!(query, Tracepoint::MultipleExpModifiers(explanations));
                query.result_cause = Some(ErrorCause::InvalidSpfRecordSyntax.into());
                return SpfResult::Permerror;
            }
        };

        // §4.6.2: ‘Each mechanism is considered in turn from left to right.’
        for directive in self.directives() {
            trace!(query, Tracepoint::EvaluateDirective(directive.clone()));

            let mechanism = &directive.mechanism;

            // ‘When a mechanism is evaluated, one of three things can happen:
            // it can match, not match, or return an exception.’
            match mechanism.evaluate_match(query, resolver).await {
                // ‘If it matches, processing ends and the qualifier value is
                // returned as the result of that record.’
                Ok(MatchResult::Match) => {
                    trace!(query, Tracepoint::MechanismMatch);

                    query.result_cause = Some(mechanism.clone().into());

                    use Qualifier::*;
                    let spf_result = match directive.qualifier.unwrap_or_default() {
                        Pass => SpfResult::Pass,
                        Fail => {
                            SpfResult::Fail(compute_explanation(explanation, query, resolver).await)
                        }
                        Neutral => SpfResult::Neutral,
                        Softfail => SpfResult::Softfail,
                    };

                    trace!(query, Tracepoint::DirectiveResult(spf_result.clone()));
                    return spf_result;
                }

                // ‘If it does not match, processing continues with the next
                // mechanism.’ In addition, invalid domain name expansions are
                // treated as not-match in accordance with §4.8.
                Ok(MatchResult::NoMatch) | Err(EvalError::InvalidName(_)) => {
                    trace!(query, Tracepoint::MechanismNoMatch);

                    // Any result cause from a recursive match is superseded and
                    // needs to be cleared.
                    query.result_cause = None;

                    continue;
                }

                // ‘If it returns an exception, mechanism processing ends and
                // the exception value is returned.’
                Err(e) => {
                    // No store when returning from recursive evaluations.
                    if let Some(e) = e.to_error_cause() {
                        query.result_cause = Some(e.into());
                    }

                    let spf_result = e.to_spf_result();
                    trace!(
                        query,
                        Tracepoint::MechanismErrorResult(e, spf_result.clone())
                    );
                    return spf_result;
                }
            }
        }

        // §6.1: ‘If all mechanisms fail to match, and a "redirect" modifier is
        // present’ …
        if let Some(redirect) = redirect {
            let spf_result = redirect.evaluate(query, resolver).await;
            trace!(query, Tracepoint::RedirectResult(spf_result.clone()));
            return spf_result;
        }

        // §4.7: ‘If none of the mechanisms match and there is no "redirect"
        // modifier, then the check_host() returns a result of "neutral"’.
        trace!(query, Tracepoint::NeutralResult);
        SpfResult::Neutral
    }
}

fn get_redirect(record: &Record) -> Result<Option<&Redirect>, Vec<Redirect>> {
    let mut redirects = record.modifiers().filter_map(|modifier| match modifier {
        Modifier::Redirect(r) => Some(r),
        _ => None,
    });

    match redirects.next() {
        None => Ok(None),
        Some(redirect) => {
            let mut rest = redirects.cloned().collect::<Vec<_>>();
            match *rest {
                [] => Ok(Some(redirect)),
                [..] => {
                    rest.insert(0, redirect.clone());
                    Err(rest)
                }
            }
        }
    }
}

fn get_explanation(record: &Record) -> Result<Option<&Explanation>, Vec<Explanation>> {
    let mut explanations = record.modifiers().filter_map(|modifier| match modifier {
        Modifier::Explanation(e) => Some(e),
        _ => None,
    });

    match explanations.next() {
        None => Ok(None),
        Some(explanation) => {
            let mut rest = explanations.cloned().collect::<Vec<_>>();
            match *rest {
                [] => Ok(Some(explanation)),
                [..] => {
                    rest.insert(0, explanation.clone());
                    Err(rest)
                }
            }
        }
    }
}

async fn compute_explanation(
    explanation: Option<&Explanation>,
    query: &mut Query<'_>,
    resolver: &Resolver<'_>,
) -> ExplanationString {
    // §6.2: ‘During recursion into an "include" mechanism, an "exp" modifier
    // from the <target-name> MUST NOT be used.’
    if query.state.is_included_query() {
        return Default::default(); // placeholder value, to be discarded
    }

    reset_state(query);

    match explanation {
        None => {
            let explanation_string = default_explanation(query, resolver).await;
            trace!(
                query,
                Tracepoint::DefaultExplanation(explanation_string.clone())
            );
            explanation_string
        }
        Some(explanation) => match explanation.evaluate_to_string(query, resolver).await {
            Ok(explanation) => {
                let explanation_string = ExplanationString::Dns(explanation);
                trace!(
                    query,
                    Tracepoint::ExplanationStringResult(explanation_string.clone())
                );
                explanation_string
            }
            Err(e) => {
                let explanation_string = default_explanation(query, resolver).await;
                trace!(
                    query,
                    Tracepoint::ExplanationStringErrorResult(e, explanation_string.clone())
                );
                explanation_string
            }
        },
    }
}

// §4.6.4: ‘the "exp" modifier -- do[es] not cause DNS queries at the time of
// SPF evaluation (the "exp" modifier only causes a lookup at a later time), and
// [its] use is not subject to this limit.’
fn reset_state(query: &mut Query) {
    // As computing the explanation is the very last thing done in a query, the
    // lookup counts are simply unconditionally reset here. Limits remain in
    // effect but are never exceeded in normal circumstances.
    query.state.reset_for_explain_string();
}

async fn default_explanation(query: &mut Query<'_>, resolver: &Resolver<'_>) -> ExplanationString {
    match query.config.default_explanation() {
        None => Default::default(),
        Some(explain_string) => {
            // Computing the default explanation should be strictly local (no
            // DNS). For this reason, the explain string is sanitised by
            // replacing all uses of `%{p}` with the default value `unknown`.
            use ExplainStringSegment::*;
            let s = explain_string
                .clone()
                .into_iter()
                .map(|segment| match segment {
                    MacroString(macro_string) => MacroString(sanitize_macro_string(macro_string)),
                    segment => segment,
                })
                .collect::<ExplainString>()
                .evaluate_to_string(query, resolver)
                .await
                .expect("failed to evaluate sanitized explain string");
            ExplanationString::Default(s)
        }
    }
}

fn sanitize_macro_string(macro_string: MacroString) -> MacroString {
    macro_string
        .into_iter()
        .map(|segment| match segment {
            MacroStringSegment::MacroExpand(MacroExpand::Macro(m))
                if m.kind() == MacroKind::ValidatedDomain =>
            {
                MacroLiteral::new(macros::default_validated_domain_string())
                    .expect("invalid default validated domain")
                    .into()
            }
            segment => segment,
        })
        .collect()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn get_sole_redirect_ok() {
        assert!(get_redirect(
            &"v=spf1 redirect=one.example.org redirect=two.example.org"
                .parse()
                .unwrap()
        )
        .is_err());
    }

    #[test]
    fn sanitize_macro_string_ok() {
        let ms = MacroString::from(vec![
            MacroStringSegment::MacroExpand(MacroExpand::Macro(MacroKind::Domain.into())),
            MacroStringSegment::MacroLiteral(MacroLiteral::new("x").unwrap()),
            MacroStringSegment::MacroExpand(MacroExpand::Macro({
                let mut m = Macro::new(MacroKind::ValidatedDomain);
                m.set_url_encode(true);
                m
            })),
        ]);

        assert_eq!(ms.to_string(), "%{d}x%{P}");

        let ms = sanitize_macro_string(ms);

        assert_eq!(ms.to_string(), "%{d}xunknown");
    }
}
