use super::*;
use crate::{
    config::Config,
    name::Name,
    record::Record,
    resolve::{Lookup, LookupError, LookupResult},
    trace::{Trace, Tracepoint},
};
use std::{
    error::Error,
    fmt::{self, Display, Formatter},
    mem,
    net::{IpAddr, Ipv4Addr, Ipv6Addr},
    time::{Duration, Instant},
};

pub struct Resolver<'l> {
    lookup: &'l (dyn Lookup + Sync + Send),
    started: Instant,
}

impl<'l> Resolver<'l> {
    pub fn new(lookup: &'l (impl Lookup + Send + Sync)) -> Self {
        Self {
            lookup,
            started: Instant::now(),
        }
    }

    pub async fn lookup_a(
        &self,
        query: &mut Query<'_>,
        name: &Name,
    ) -> LookupResult<Vec<Ipv4Addr>> {
        trace!(query, Tracepoint::LookupA(name.clone()));
        self.check_timeout(query.config.timeout())?;
        self.lookup.lookup_a(name).await
    }

    pub async fn lookup_aaaa(
        &self,
        query: &mut Query<'_>,
        name: &Name,
    ) -> LookupResult<Vec<Ipv6Addr>> {
        trace!(query, Tracepoint::LookupAaaa(name.clone()));
        self.check_timeout(query.config.timeout())?;
        self.lookup.lookup_aaaa(name).await
    }

    pub async fn lookup_a_or_aaaa(
        &self,
        query: &mut Query<'_>,
        name: &Name,
        ip: IpAddr,
    ) -> LookupResult<Vec<IpAddr>> {
        // §5: ‘When any mechanism fetches host addresses to compare with <ip>,
        // when <ip> is an IPv4, "A" records are fetched; when <ip> is an IPv6
        // address, "AAAA" records are fetched.’
        match ip {
            IpAddr::V4(_) => self
                .lookup_a(query, name)
                .await
                .map(|addrs| addrs.into_iter().map(From::from).collect()),
            IpAddr::V6(_) => self
                .lookup_aaaa(query, name)
                .await
                .map(|addrs| addrs.into_iter().map(From::from).collect()),
        }
    }

    pub async fn lookup_mx(&self, query: &mut Query<'_>, name: &Name) -> LookupResult<Vec<Name>> {
        trace!(query, Tracepoint::LookupMx(name.clone()));
        self.check_timeout(query.config.timeout())?;
        self.lookup.lookup_mx(name).await
    }

    pub async fn lookup_txt(
        &self,
        query: &mut Query<'_>,
        name: &Name,
    ) -> LookupResult<Vec<String>> {
        trace!(query, Tracepoint::LookupTxt(name.clone()));
        self.check_timeout(query.config.timeout())?;
        self.lookup.lookup_txt(name).await
    }

    pub async fn lookup_ptr(&self, query: &mut Query<'_>, ip: IpAddr) -> LookupResult<Vec<Name>> {
        trace!(query, Tracepoint::LookupPtr(ip));
        self.check_timeout(query.config.timeout())?;
        self.lookup.lookup_ptr(ip).await
    }

    fn check_timeout(&self, timeout: Duration) -> LookupResult<()> {
        // `LookupError::Timeout` may also be returned from the `Lookup`
        // implementation. They are not distinguished during evaluation.
        if self.started.elapsed() > timeout {
            Err(LookupError::Timeout)
        } else {
            Ok(())
        }
    }
}

#[derive(Debug)]
pub struct Query<'c> {
    pub config: &'c Config,
    pub params: QueryParams,
    pub state: QueryState,
    pub result_cause: Option<SpfResultCause>,
    pub trace: Trace,
}

impl<'c> Query<'c> {
    pub fn new(config: &'c Config, params: QueryParams, state: QueryState) -> Self {
        Self {
            config,
            params,
            state,
            result_cause: Default::default(),
            trace: Default::default(),
        }
    }

    pub async fn execute(&mut self, resolver: &Resolver<'_>) -> SpfResult {
        trace!(self, Tracepoint::ExecuteQuery(self.params.domain.clone()));

        // This use of `clone` only to satisfy the borrow checker.
        let domain = self.params.domain.clone();

        use SpfRecordLookupError::*;
        let record = match lookup_spf_record(resolver, self, &domain).await {
            Ok(r) => r,

            // §4.4: ‘If the DNS lookup returns a server failure (RCODE 2) or
            // some other error (RCODE other than 0 or 3), or if the lookup
            // times out, then check_host() terminates immediately with the
            // result "temperror".’
            Err(DnsLookup(e)) => {
                trace!(self, Tracepoint::SpfRecordLookupError(e));
                self.result_cause = Some(ErrorCause::Dns.into());
                return SpfResult::Temperror;
            }

            // §4.5: ‘If the resultant record set includes no records,
            // check_host() produces the "none" result.’
            Err(NoRecord) => {
                trace!(self, Tracepoint::NoSpfRecord);
                return SpfResult::None;
            }

            // §4.5: ‘If the resultant record set includes more than one record,
            // check_host() produces the "permerror" result.’
            Err(MultipleRecords(records)) => {
                trace!(self, Tracepoint::MultipleSpfRecords(records));
                self.result_cause = Some(ErrorCause::MultipleSpfRecords.into());
                return SpfResult::Permerror;
            }

            // §4.6: ‘if there are any syntax errors anywhere in the record,
            // check_host() returns immediately with the result "permerror",
            // without further interpretation or evaluation.’
            Err(Syntax(s)) => {
                trace!(self, Tracepoint::InvalidSpfRecordSyntax(s));
                self.result_cause = Some(ErrorCause::InvalidSpfRecordSyntax.into());
                return SpfResult::Permerror;
            }
        };

        record.evaluate(self, resolver).await
    }
}

#[derive(Debug)]
pub enum SpfRecordLookupError {
    DnsLookup(LookupError),
    NoRecord,
    MultipleRecords(Vec<String>),
    Syntax(String),
}

impl Error for SpfRecordLookupError {}

impl Display for SpfRecordLookupError {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "failed to obtain SPF record")
    }
}

impl From<LookupError> for SpfRecordLookupError {
    fn from(error: LookupError) -> Self {
        match error {
            LookupError::NoRecords => Self::NoRecord,
            _ => Self::DnsLookup(error),
        }
    }
}

async fn lookup_spf_record(
    resolver: &Resolver<'_>,
    query: &mut Query<'_>,
    name: &Name,
) -> Result<Record, SpfRecordLookupError> {
    let mut records = resolver
        .lookup_txt(query, name)
        .await?
        .into_iter()
        .filter(|s| is_spf_record(s));

    use SpfRecordLookupError::*;
    match records.next() {
        None => Err(NoRecord),
        Some(record) => {
            let mut rest = records.collect::<Vec<_>>();
            match *rest {
                [] => record.parse().map_err(|_| Syntax(record)),
                [..] => {
                    rest.insert(0, record);
                    Err(MultipleRecords(rest))
                }
            }
        }
    }
}

// §4.5: ‘Starting with the set of records that were returned by the lookup,
// discard records that do not begin with a version section of exactly
// "v=spf1".’
pub(crate) fn is_spf_record(s: &str) -> bool {
    let prefix = b"v=spf1";
    let b = s.as_bytes();
    b.len() >= prefix.len()
        && b[..prefix.len()].eq_ignore_ascii_case(prefix)
        && (b.len() == prefix.len() || b[prefix.len()] == b' ')
}

#[derive(Clone, Debug, Eq, Hash, PartialEq)]
pub struct QueryParams {
    ip: IpAddr,
    domain: Name,
    sender: Sender,
    helo_domain: String,
}

impl QueryParams {
    pub fn new(ip: IpAddr, domain: Name, sender: Sender, helo_domain: String) -> Self {
        Self {
            ip,
            domain,
            sender,
            helo_domain,
        }
    }

    pub fn ip(&self) -> IpAddr {
        self.ip
    }

    pub fn domain(&self) -> &Name {
        &self.domain
    }

    pub fn sender(&self) -> &Sender {
        &self.sender
    }

    pub fn helo_domain(&self) -> &str {
        &self.helo_domain
    }

    pub fn replace_domain(&mut self, domain: Name) -> Name {
        mem::replace(&mut self.domain, domain)
    }
}

#[derive(Clone, Copy, Debug, Eq, Hash, PartialEq)]
pub struct ParseSenderError;

impl Error for ParseSenderError {}

impl Display for ParseSenderError {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "could not parse sender")
    }
}

#[derive(Clone, Debug, Eq, Hash, PartialEq)]
pub struct Sender {
    local_part: String,
    domain_part: Name,
}

impl Sender {
    pub fn new(sender: &str) -> Result<Self, ParseSenderError> {
        let (local, domain) = match sender.rfind('@') {
            None => {
                // §4.3: ‘If the <sender> has no local-part, substitute the
                // string "postmaster" for the local-part.’
                ("postmaster", sender)
            }
            Some(index) => {
                let (local, domain) = sender.split_at(index);
                if !is_local_part(local) {
                    return Err(ParseSenderError);
                }
                (local, &domain[1..])
            }
        };

        match Name::domain(domain) {
            Ok(domain) => Ok(Self {
                local_part: local.to_owned(),
                domain_part: domain,
            }),
            Err(_) => Err(ParseSenderError),
        }
    }

    pub fn local_part(&self) -> &str {
        &self.local_part
    }

    pub fn domain_part(&self) -> &Name {
        &self.domain_part
    }
}

impl Display for Sender {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "{}@{}", self.local_part, self.domain_part)
    }
}

// ‘local-part’ is defined in RFC 5321, §4.1.2. Modifications for
// internationalisation are in RFC 6531, §3.3. An older summary can be found in
// RFC 3696, §3.
fn is_local_part(s: &str) -> bool {
    // See RFC 5321, §4.5.3.1.1.
    if s.len() > 64 {
        return false;
    }

    if s.starts_with('"') {
        is_quoted_string(s)
    } else {
        is_dot_string(s)
    }
}

fn is_quoted_string(s: &str) -> bool {
    fn is_qtext_smtp(c: char) -> bool {
        c == ' ' || c.is_ascii_graphic() && !matches!(c, '"' | '\\') || !c.is_ascii()
    }

    if s.starts_with('"') && s.ends_with('"') && s.len() >= 2 {
        let mut quoted = false;
        for c in s[1..(s.len() - 1)].chars() {
            if quoted {
                if c == ' ' || c.is_ascii_graphic() {
                    quoted = false;
                } else {
                    return false;
                }
            } else if c == '\\' {
                quoted = true;
            } else if !is_qtext_smtp(c) {
                return false;
            }
        }
        !quoted
    } else {
        false
    }
}

fn is_dot_string(s: &str) -> bool {
    // See RFC 5322, §3.2.3, with the modifications in RFC 6531, §3.3.
    fn is_atext(c: char) -> bool {
        c.is_ascii_alphanumeric()
            || matches!(
                c,
                '!' | '#'
                    | '$'
                    | '%'
                    | '&'
                    | '\''
                    | '*'
                    | '+'
                    | '-'
                    | '/'
                    | '='
                    | '?'
                    | '^'
                    | '_'
                    | '`'
                    | '{'
                    | '|'
                    | '}'
                    | '~'
            )
            || !c.is_ascii()
    }

    let mut dot = true;
    for c in s.chars() {
        if dot {
            if is_atext(c) {
                dot = false;
            } else {
                return false;
            }
        } else if c == '.' {
            dot = true;
        } else if !is_atext(c) {
            return false;
        }
    }
    !dot
}

const MAX_LOOKUPS: usize = 10;

#[derive(Clone, Debug, Default, Eq, Hash, PartialEq)]
pub struct QueryState {
    lookup_count: usize,
    void_lookup_count: usize,
    included_query: bool,
    eval_explain_string: bool,
}

impl QueryState {
    pub fn new() -> Self {
        Default::default()
    }

    pub fn increment_lookup_count(&mut self) -> EvalResult<()> {
        if self.lookup_count < MAX_LOOKUPS {
            self.lookup_count += 1;
            Ok(())
        } else {
            Err(EvalError::LookupLimitExceeded)
        }
    }

    pub fn increment_void_lookup_count(&mut self, max: usize) -> EvalResult<()> {
        if self.void_lookup_count < max {
            self.void_lookup_count += 1;
            Ok(())
        } else {
            Err(EvalError::VoidLookupLimitExceeded)
        }
    }

    pub fn is_included_query(&self) -> bool {
        self.included_query
    }

    pub fn set_included_query(&mut self, included: bool) {
        self.included_query = included;
    }

    pub fn is_eval_explain_string(&self) -> bool {
        self.eval_explain_string
    }

    pub fn reset_for_explain_string(&mut self) {
        self.lookup_count = Default::default();
        self.void_lookup_count = Default::default();
        self.eval_explain_string = true;
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn sender_from_email_address() {
        let sender = Sender::new("amy@example.org").unwrap();
        assert_eq!(sender.local_part(), "amy");
        assert_eq!(sender.domain_part().to_string(), "example.org");
    }

    #[test]
    fn sender_from_domain() {
        let sender = Sender::new("mail.example.org").unwrap();
        assert_eq!(sender.local_part(), "postmaster");
        assert_eq!(sender.domain_part().to_string(), "mail.example.org");
    }

    #[test]
    fn is_local_part_ok() {
        assert!(is_local_part(
            "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
        ));
        assert!(!is_local_part(
            "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
        ));

        assert!(is_local_part(r#""""#));
        assert!(!is_local_part(r#"""#));
        assert!(is_local_part(r#""xy""#));
        assert!(is_local_part(r#""𝔵𝔶""#));
        assert!(is_local_part(r#""@""#));
        assert!(is_local_part(r#""x y""#));
        assert!(is_local_part(r#""\x""#));
        assert!(!is_local_part(r#""\𝔵""#));
        assert!(is_local_part(r#""\"""#));
        assert!(!is_local_part(r#""x\""#));
        assert!(!is_local_part(r#"""""#));

        assert!(!is_local_part(""));
        assert!(!is_local_part("."));
        assert!(!is_local_part(".x"));
        assert!(!is_local_part("x."));
        assert!(is_local_part("x"));
        assert!(is_local_part("xy"));
        assert!(is_local_part("xy.z"));
        assert!(is_local_part("xy.z+tag"));
        assert!(!is_local_part("xy..z"));
        assert!(is_local_part("겫13.12겫"));
    }

    #[test]
    fn is_local_part_rfc3696_ok() {
        // Examples from RFC 3696, §3 (with errata!).
        assert!(is_local_part(r#""Abc\@def""#));
        assert!(is_local_part(r#""Fred\ Bloggs""#));
        assert!(is_local_part(r#""Joe.\\Blow""#));
        assert!(is_local_part(r#""Abc@def""#));
        assert!(is_local_part(r#""Fred Bloggs""#));

        assert!(is_local_part("user+mailbox"));
        assert!(is_local_part("customer/department=shipping"));
        assert!(is_local_part("$A12345"));
        assert!(is_local_part("!def!xyz%abc"));
        assert!(is_local_part("_somename"));
    }
}
