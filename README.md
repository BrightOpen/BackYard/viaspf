# viaspf

A library implementing the *Sender Policy Framework* (SPF) protocol, version
1, as specified in [RFC 7208].

This library contains a complete implementation of the SPF protocol.
However, no DNS resolution functionality is included in the library.
Instead, DNS resolution is abstracted into the trait [`Lookup`], an
implementation of which must be provided by library consumers when
initiating an SPF query.

## Usage

The function [`viaspf::evaluate_spf`][`evaluate_spf`] is the main public API
item. This function corresponds to the [*check_host()*] abstraction of RFC
7208. The function signature differs from the one given in the
specification, but it produces results that are semantically equivalent and
thus fulfil the requirements for a compliant SPF implementation.

`evaluate_spf` takes five arguments: the first two arguments control
properties of protocol handling, the remaining arguments cover the inputs of
the *check_host()* function. The result type conveys the SPF result. Refer
to the API documentation for the complete reference.

The function’s first argument, `lookup`, is a reference of a type that
implements [`viaspf::Lookup`][`Lookup`]. This implementation must be
provided by API consumers; it functions as the core DNS resolution facility
through which all DNS queries are performed. See the `Lookup` documentation
for implementation requirements. A sample implementation can be found in the
included example application.

Once a working implementation of `Lookup` is available, authorising a host
is straightforward:

```rust
let lookup = MyLookup;
let config = Default::default();

let result = evaluate_spf(
    &lookup,
    &config,
    IpAddr::from([9, 9, 9, 9]),
    "amy@example.org",
    "mail.example.org",
).await;

println!("spf={}", result.result);
```

Users of this library will want to refer to the API documentation of
[`Lookup`] in order to get started.

[RFC 7208]: https://www.rfc-editor.org/rfc/rfc7208
[`Lookup`]: trait.Lookup.html
[`evaluate_spf`]: fn.evaluate_spf.html
[*check_host()*]: https://www.rfc-editor.org/rfc/rfc7208#section-4

License: GPL-3.0-or-later
